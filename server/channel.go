package server

import (
	"fmt"
)

type Channel struct {
	Name      string
	Sessions  []*Session
	Create    int64
	Topic     string
	TopicTime int64
	TopicWho  string
}

func (c *Channel) WriteAll(data string) {
	for _, session := range c.Sessions {
		session.Write(data)
	}
}

func (c *Channel) WriteAllWithHost(data string, host string) {
	c.WriteAll(fmt.Sprintf(":%s %s", host, data))
}

func (c *Channel) WriteAllWithHostExcept(data string, host string, except *Session) {
	for _, session := range c.Sessions {
		if session == except {
			continue
		}
		session.Write(fmt.Sprintf(":%s %s", host, data))
	}
}

func (c *Channel) WriteAllMsgWithTagsExcept(data string, host string, except *Session, tags []string) {
	for _, session := range c.Sessions {
		if session == except {
			continue
		}
		session.WriteWithTags(fmt.Sprintf(":%s %s", host, data), tags)
	}
}

func (c *Channel) Join(session *Session) {
	if !c.IsInChannel(session) {
		session.Channels = append(session.Channels, c)
		c.Sessions = append(c.Sessions, session)
		c.WriteAllWithHost("JOIN :"+c.Name, session.FullHostname())
		c.SendTopic(session)
		session.WriteSNN(RPL_NAMREPLY, c.NamesString())
		session.WriteSNNC(RPL_ENDOFNAMES, c, "End of /NAMES list.")
	}
}

func (c *Channel) Leave(session *Session, kind string, data string) {
	if c.IsInChannel(session) {
		if kind == "PART" {
			if data == "" {
				c.WriteAllWithHost(fmt.Sprintf("%s %s", kind, c.Name), session.FullHostname())
			} else {
				c.WriteAllWithHost(fmt.Sprintf("%s %s :%s", kind, c.Name, data), session.FullHostname())
			}
		} else if kind == "QUIT" {
			c.WriteAllWithHost("QUIT :"+data, session.FullHostname())
		}

		i := 0
		for _, s := range c.Sessions {
			if s != session {
				c.Sessions[i] = s
				i++
			}
		}
		c.Sessions = c.Sessions[:i]

		i = 0
		for _, sc := range session.Channels {
			if sc != c {
				session.Channels[i] = sc
				i++
			}
		}
		session.Channels = session.Channels[:i]
	}
}

func (c *Channel) IsInChannel(session *Session) bool {
	for _, s := range c.Sessions {
		if s == session {
			return true
		}
	}
	return false
}

func (c *Channel) SendTopic(session *Session) {
	if c.Topic == "" {
		session.WriteSNNC(RPL_NOTOPIC, c, "No topic is set.")
		return
	}

	session.WriteSNNC(RPL_TOPIC, c, c.Topic)
	session.WriteSNN(RPL_TOPICTIME, fmt.Sprintf("%s %s %d", c.Name, c.TopicWho, c.TopicTime))
}

func (c *Channel) NamesString() string {
	out := "= " + c.Name + " :"
	for _, session := range c.Sessions {
		out += session.Nickname + " "
	}
	return out[:len(out)-1]
}

func (c *Channel) ModesString() string {
	return "+n"
}
