package server

type Config struct {
	Network string
	Server  string
	Listen  []ListenConfig
	HTTP    string
	Debug   bool
}

type ListenConfig struct {
	Bind string
	TLS  bool
	Cert string
	Key  string
}
