package server

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"strings"
	"time"
)

type Fantasy struct {
	fortuneList []string
}

func (f *Fantasy) LoadFortunes() {
	data, err := ioutil.ReadFile("fortune.txt")
	if err != nil {
		log.Println("unable to load fortune list:", err)
		return
	}

	f.fortuneList = []string{}

	fortuneData := strings.Split(string(data), "\n%\n")

	for _, fortune := range fortuneData {
		f.fortuneList = append(f.fortuneList, strings.Replace(fortune, "\t", "    ", -1))
	}

	if fortuneCount := len(f.fortuneList); fortuneCount > 0 {
		log.Println("[fantasy] loaded", fortuneCount, "fortunes.")
	}
}

func (f *Fantasy) GetFortune() string {
	if len(f.fortuneList) == 0 {
		return "?"
	}
	rand.Seed(time.Now().UTC().UnixNano())
	return f.fortuneList[rand.Int()%len(f.fortuneList)]
}

func (f *Fantasy) WriteFortuneNotice(session *Session) {
	for _, line := range strings.Split(f.GetFortune(), "\n") {
		session.WriteSN("NOTICE", "*", ":"+line)
	}
}

func (f *Fantasy) HandleFortune(session *Session, args []string) {
	if len(args) > 1 {
		target := args[1]
		if target == "rehash" {
			f.LoadFortunes()
		}

		if !strings.HasPrefix(target, "#") {
			return
		}
		channel := session.Server.FindChannel(target)
		if channel == nil {
			return
		}
		if channel.IsInChannel(session) {
			for _, line := range strings.Split(f.GetFortune(), "\n") {
				channel.WriteAllWithHost(fmt.Sprintf("PRIVMSG %s :%s", target, line), session.FullHostname())
			}
		}
	} else {
		f.WriteFortuneNotice(session)
	}
}

func NewFantasy() *Fantasy {
	f := &Fantasy{}
	f.LoadFortunes()
	return f
}
