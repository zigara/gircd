package server

import (
	"crypto/sha512"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	VERSION    = "gIRCd 0.2"
	UserModes  = "aosDRZ"
	ChanModes  = "bgiklmnoprstvz"
	ChanModesP = "bklov"           // channel modes with parameters
	ChanModesS = "b,k,l,gimnprstz" // used in RPL_ISUPPORT
)

// user modes:
// a = administrator
// o = operator
// s = service
// D = deaf
// R = authenticated messages only
// Z = tls connection

// channel modes:
// b = ban
// g = allow anyone to invite
// i = invite only
// k = key
// l = limit
// m = moderate
// n = no external messages
// o = operator
// p = private
// r = authenticated users only
// s = secret
// t = topic lock
// v = voice
// z = tls only

type Message struct {
	Data    string
	Session *Session
}

type Listener struct {
	Listen net.Listener
	Config ListenConfig
	Closed bool
}

func NewGIRCd(c Config) *GIRCd {
	return &GIRCd{
		Sessions:    make([]*Session, 0),
		Channels:    make(map[string]*Channel, 0),
		Create:      time.Now(),
		Fantasy:     NewFantasy(),
		Config:      c,
		newSession:  make(chan *Session, 0),
		delSession:  make(chan *Session, 0),
		messageChan: make(chan Message, 0),
	}
}

type GIRCd struct {
	Sessions    []*Session
	Channels    map[string]*Channel
	Create      time.Time
	Fantasy     *Fantasy
	Config      Config
	listeners   []*Listener
	listenersMu sync.Mutex
	newSession  chan *Session
	delSession  chan *Session
	messageChan chan Message
}

func (g *GIRCd) Process() {
	ticker := time.NewTicker(time.Minute * 1)
	for {
		select {
		case session := <-g.newSession:
			log.Println("new session from", session.Conn.RemoteAddr())
			g.Sessions = append(g.Sessions, session)
			go session.Process()
			session.WriteIntro()

		case session := <-g.delSession:
			log.Println("closing session from", session.Conn.RemoteAddr())
			var sl []*Session
			var cl []*Channel
			for _, channel := range session.Channels {
				for _, cs := range channel.Sessions {
					if !sessionInSlice(cs, sl) && session != cs {
						sl = append(sl, cs)
					}
				}
			}

			cl = append(cl, session.Channels...)
			for _, channel := range cl {
				channel.Leave(session, "", "")
			}

			for _, s := range sl {
				s.Write(fmt.Sprintf(":%s QUIT :disconnected", session.FullHostname()))
			}

			i := 0
			for _, s := range g.Sessions {
				if s != session {
					g.Sessions[i] = s
					i++
				}
			}
			g.Sessions = g.Sessions[:i]

		case message := <-g.messageChan:
			g.ProcessMessage(message)

		case <-ticker.C:
			for _, s := range g.Sessions {
				s.Write("PING " + g.Config.Server)
			}
		}
	}
}

func (g *GIRCd) ProcessMessage(message Message) {
	session, data := message.Session, message.Data
	args := strings.Split(data, " ")
	argCount := len(args)

	if g.Config.Debug {
		log.Printf("[>] [%s] [%s] %s\n", session.Conn.RemoteAddr(), session.Nickname, data)
	}

	if argCount > 1 {
		switch strings.ToUpper(args[0]) {
		case "PING":
			g.HandlePing(session, args)
			return
		case "CAP":
			g.HandleCap(session, args)
			return
		}
	}

	if !session.Valid {
		if argCount > 1 {
			switch strings.ToUpper(args[0]) {
			case "NICK":
				g.HandleNick(session, args)
			case "USER":
				session.User = args[1]
			case "JOIN":
				session.WriteUnregistered("JOIN")
			}
		}

		if session.User != "" && session.Nickname != "" {
			t, ok := session.Conn.(*tls.Conn)
			if ok && len(t.ConnectionState().PeerCertificates) > 0 {
				session.Fingerprint = fmt.Sprintf("%x", sha512.Sum512(t.ConnectionState().PeerCertificates[0].Raw))
			}
			session.Valid = true
			session.WriteWelcome()
			return
		}
	}

	if session.Valid {
		cmd := strings.ToUpper(args[0])

		switch cmd {
		case "QUIT":
			session.CloseConn()
			return
		case "FORTUNE":
			g.Fantasy.HandleFortune(session, args)
			return
		case "FINGERPRINT":
			g.HandleFingerprint(session, args)
			return
		}

		if argCount > 1 {
			switch cmd {
			case "NICK":
				g.HandleNick(session, args)

			case "JOIN":
				g.HandleJoin(session, args)

			case "PART":
				g.HandlePart(session, args, data)

			case "NAMES":
				g.HandleNames(session, args)

			case "WHO":
				g.HandleWho(session, args)

			case "MODE":
				g.HandleMode(session, args)

			case "TOPIC":
				g.HandleTopic(session, args, data)

			case "PRIVMSG":
				if argCount > 2 {
					g.HandleMsg(cmd, session, args, data)
				}

			case "NOTICE":
				if argCount > 2 {
					g.HandleMsg(cmd, session, args, data)
				}
			}
		}
	}
}

func (g *GIRCd) FindChannel(name string) *Channel {
	if channel, ok := g.Channels[strings.ToLower(name)]; ok {
		return channel
	}
	return nil
}

func (g *GIRCd) IsNickUsable(nick string) bool {
	nick = strings.ToLower(nick)
	for _, session := range g.Sessions {
		if strings.ToLower(session.Nickname) == nick {
			return false
		}
	}
	return true
}

func (g *GIRCd) FindSession(nick string) *Session {
	nick = strings.ToLower(nick)
	for _, session := range g.Sessions {
		if strings.ToLower(session.Nickname) == nick {
			return session
		}
	}
	return nil
}

func (g *GIRCd) Listen(l ListenConfig) error {
	var listen net.Listener

	if l.TLS {
		cert, err := tls.LoadX509KeyPair(l.Cert, l.Key)
		if err != nil {
			return err
		}

		config := tls.Config{
			Certificates: []tls.Certificate{cert},
			ClientAuth:   tls.RequestClientCert,
		}

		listen, err = tls.Listen("tcp", l.Bind, &config)
		if err != nil {
			return err
		}
	} else {
		var err error
		listen, err = net.Listen("tcp", l.Bind)
		if err != nil {
			return err
		}
	}

	msg := "listening on " + listen.Addr().String()
	if l.TLS {
		msg += " (tls)"
	}
	log.Println(msg)

	listener := &Listener{Config: l, Listen: listen}
	g.listenersMu.Lock()
	g.listeners = append(g.listeners, listener)
	g.listenersMu.Unlock()

	go func() {
		defer func() {
			g.listenersMu.Lock()
			g.listeners = Delete(g.listeners, listener)
			g.listenersMu.Unlock()
		}()
		for {
			conn, err := listen.Accept()
			if err != nil {
				if !errors.Is(err, net.ErrClosed) {
					log.Println("error accepting session:", err)
				}
				break
			}

			host := "unknown"
			if l.TLS {
				host = "unknown/tls"
			}

			g.newSession <- &Session{
				Conn:      conn,
				Server:    g,
				Host:      host,
				TLS:       l.TLS,
				writeChan: make(chan []byte, 4096),
				closeChan: make(chan bool, 4),
			}
		}
	}()

	return nil
}

func (g *GIRCd) Reload() {
	g.listenersMu.Lock()
	for _, l := range g.listeners {
		if l.Closed {
			continue
		}
		l.Closed = true
		if err := l.Listen.Close(); err != nil {
			log.Printf("unable to close listener: %v", err)
		}
		log.Printf("closed listener on %v", l.Listen.Addr().String())
	}
	g.listenersMu.Unlock()

	for _, l := range g.Config.Listen {
		if err := g.Listen(l); err != nil {
			log.Printf("unable to start listener on %v: %v", l.Bind, err)
		}
	}
}

func Start() error {
	var c Config
	cf, err := os.Open("config.json")
	if err != nil {
		return fmt.Errorf("error reading configuration file: %w", err)
	}
	if err := json.NewDecoder(cf).Decode(&c); err != nil {
		return fmt.Errorf("error decoding configuration file: %w", err)
	}
	_ = cf.Close()

	g := NewGIRCd(c)
	go g.StartHTTP()

	for _, l := range c.Listen {
		if err := g.Listen(l); err != nil {
			return err
		}
	}

	g.Process()
	return nil
}
