package server

import (
	"encoding/json"
	"log"
	"net/http"
)

func (g *GIRCd) StartHTTP() {
	type GenericResponse struct {
		Success bool   `json:"success"`
		Error   string `json:"error,omitempty"`
	}
	http.HandleFunc("/sessions", func(w http.ResponseWriter, r *http.Request) {
		type SessionEntry struct {
			Nick        string `json:"nick"`
			Host        string `json:"host"`
			Addr        string `json:"addr"`
			Fingerprint string `json:"fingerprint,omitempty"`
		}
		type SessionResponse struct {
			Count    int            `json:"count"`
			Sessions []SessionEntry `json:"sessions"`
		}

		t := SessionResponse{0, []SessionEntry{}}
		for _, s := range g.Sessions {
			t.Sessions = append(t.Sessions, SessionEntry{
				Nick:        s.Nickname,
				Host:        s.FullHostname(),
				Addr:        s.Conn.RemoteAddr().String(),
				Fingerprint: s.Fingerprint,
			})
		}
		t.Count = len(t.Sessions)

		if err := json.NewEncoder(w).Encode(&t); err != nil {
			log.Println("error encoding sessions json response:", err)
		}
	})

	http.HandleFunc("/reload", func(w http.ResponseWriter, r *http.Request) {
		g.Reload()
		if err := json.NewEncoder(w).Encode(&GenericResponse{Success: true}); err != nil {
			log.Println("error encoding json response:", err)
		}
	})

	if err := http.ListenAndServe(g.Config.HTTP, nil); err != nil {
		log.Fatalln("fatal error: http server error:", err)
	}
}
