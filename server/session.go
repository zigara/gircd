package server

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

type Session struct {
	Nickname     string
	User         string
	Host         string
	Conn         net.Conn
	Server       *GIRCd
	Valid        bool
	TLS          bool
	Channels     []*Channel
	Capabilities Capabilities
	Fingerprint  string
	writeChan    chan []byte
	closeChan    chan bool
	quit         bool
}

type Capabilities struct {
	MessageTags bool
}

const deadline = time.Minute * 5

func (session *Session) Process() {
	defer session.Close()

	go func() {
		defer session.Close()
		for {
			select {
			case data := <-session.writeChan:
				if data == nil {
					return
				}
				if err := session.Conn.SetWriteDeadline(time.Now().Add(deadline)); err != nil {
					return
				}
				_, err := session.Conn.Write(data)
				if err != nil {
					log.Println("error writing data:", err)
					return
				}
			}
		}
	}()

	scanner := bufio.NewScanner(session.Conn)
	scanner.Buffer(make([]byte, 512), 1024*32)
	setDeadline := func() error {
		return session.Conn.SetReadDeadline(time.Now().Add(deadline))
	}
	if err := setDeadline(); err != nil {
		return
	}

	// TODO: clean up deadlines
	// TODO: check for scanner errors
	for scanner.Scan() {
		session.Server.messageChan <- Message{Data: scanner.Text(), Session: session}
		if err := setDeadline(); err != nil {
			return
		}
	}
	if err := scanner.Err(); err != nil {
		log.Println("error reading data:", err)
	}
}

func (session *Session) FullHostname() string {
	return fmt.Sprintf("%s!%s@%s", session.Nickname, session.User, session.Host)
}

func (session *Session) WriteIntro() {
	session.Server.Fantasy.WriteFortuneNotice(session)
}

func (session *Session) WriteWelcome() {
	config := session.Server.Config
	session.WriteSNN(RPL_WELCOME, fmt.Sprintf(":Welcome to the %s network! %s", config.Network, session.FullHostname()))
	session.WriteSNN(RPL_YOURHOST, fmt.Sprintf(":Your host is %s, running %s", config.Server, VERSION))
	session.WriteSNN(RPL_CREATED, fmt.Sprintf(":This server was created %s", session.Server.Create))
	session.WriteSNN(RPL_MYINFO, fmt.Sprintf("%s %s %s %s %s", config.Server, VERSION, UserModes, ChanModes, ChanModesP))
	session.WriteSNN(RPL_ISUPPORT, fmt.Sprintf("NETWORK=%s CASEMAPPING=rfc1459 CHANMODES=%s CHANTYPES=# PREFIX=(ov)@+ FNC :is supported by this server.", config.Network, ChanModesS))
	session.WriteSNN(RPL_ISUPPORT, "MAXCHANNELS=420 MAXTARGETS=420 MODES=15 NICKLEN=32 TOPICLEN=420 AWAYLEN=420 CHANNELLEN=420 KICKLEN=420 MAXBANS=420 :is supported by this server.")
	session.WriteSNN(RPL_MOTDSTART, fmt.Sprintf(":%s message of the day", config.Server))
	session.WriteSNN(RPL_MOTD, ":- this is the message of the day")
	session.WriteSNN(RPL_ENDOFMOTD, ":end of message of the day.")
}

func (session *Session) WriteUnregistered(s string) {
	session.WriteSNN(ERR_NOTREGISTERED, s+" :You have not registered")
}

func (session *Session) WriteSN(numeric string, nick string, data string) {
	if nick == "" {
		nick = "?"
	}
	session.Write(fmt.Sprintf(":%s %s %s %s", session.Server.Config.Server, numeric, nick, data))
}

func (session *Session) WriteSNN(numeric string, data string) {
	session.WriteSN(numeric, session.Nickname, data)
}

func (session *Session) WriteSNNT(numeric string, target string, data string) {
	session.WriteSNN(numeric, target+" :"+data)
}

func (session *Session) WriteSNNC(numeric string, channel *Channel, data string) {
	session.WriteSNNT(numeric, channel.Name, data)
}

func (session *Session) Write(data string) {
	if session.quit {
		return
	}
	if session.Server.Config.Debug {
		log.Printf("[<] [%s] [%s] %s\n", session.Conn.RemoteAddr(), session.Nickname, strings.TrimSuffix(data, "\n"))
	}
	select {
	case session.writeChan <- []byte(data + "\r\n"):
		return
	default:
		log.Println("write buffer full!", session.Nickname, session.Conn.RemoteAddr())
		session.CloseConn()
	}
}

func (session *Session) WriteWithTags(data string, tags []string) {
	if session.Capabilities.MessageTags && len(tags) > 0 {
		session.Write(fmt.Sprintf("@%s %s", strings.Join(tags, ";"), data))
	} else {
		session.Write(data)
	}
}

func (session *Session) Close() {
	if session.quit {
		return
	}

	session.quit = true
	close(session.writeChan)
	session.Server.delSession <- session
	session.CloseConn()
}

func (session *Session) CloseConn() {
	_ = session.Conn.Close()
}

func sessionInSlice(s *Session, l []*Session) bool {
	for _, session := range l {
		if session == s {
			return true
		}
	}
	return false
}
