package server

func Delete[T comparable](s []T, v T) []T {
	i := 0
	for _, vv := range s {
		if vv != v {
			s[i] = vv
			i++
		}
	}
	return s[:i]
}
